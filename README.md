nordbro-demo
============

[![pipeline status](https://gitlab.com/patrikorup/nordbro-demo/badges/master/pipeline.svg)](https://gitlab.com/patrikorup/nordbro-demo/commits/master)
[![coverage report](https://gitlab.com/patrikorup/nordbro-demo/badges/master/coverage.svg)](https://gitlab.com/patrikorup/nordbro-demo/commits/master)

# Prerequisites

* Docker [[Mac][docker-mac]]
* [Docker Compose][docker-compose] (included in Docker for Mac)

# Setup

## macOS

The easiest and most convenient way to get this demo application up and running is by running the **setup.sh** script placed in the root of the project.

```
$ cd clone-location/
$ ./setup.sh
```

Add a new entry to your hosts file that will point **nordbro-demo.dev** to **127.0.0.1**.

```
$ echo '127.0.0.1   nordbro-demo.dev' | sudo tee --append /etc/hosts
```

You should now be able to access the application at <https://nordbro-demo.dev:4431/>.


# Run tests

You can run all tests by running following command.

```
docker-compose exec php-fpm ant test
```

[docker-mac]: https://docs.docker.com/docker-for-mac/
[docker-compose]: https://docs.docker.com/compose/install/
