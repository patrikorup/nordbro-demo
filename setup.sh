#!/usr/bin/env bash

docker-compose build

build_res=${?}
if [ $build_res -ne 0 ]; then
  echo "ERROR: docker-compose build failed"
  exit $?
fi

docker-compose up -d

build_res=${?}
if [ $build_res -ne 0 ]; then
  echo "ERROR: Docker containers could not be started"
  exit $?
fi

docker-compose exec -T php-fpm ant build

build_res=${?}
if [ $build_res -ne 0 ]; then
  echo "ERROR: composer install failed"
  exit $?
fi

docker-compose exec -T php-fpm bin/console doctrine:migrations:migrate

build_res=${?}
if [ $build_res -ne 0 ]; then
  echo "ERROR: Could not run migrations."
  exit $?
fi
