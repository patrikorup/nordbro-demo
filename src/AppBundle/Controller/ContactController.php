<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Person;
use AppBundle\Form\Type\CompanyType;
use AppBundle\Form\Type\PersonType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class ContactController
{

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * ContactController constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param RouterInterface $router
     * @param \Twig_Environment $twig
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        \Twig_Environment $twig
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->twig = $twig;
    }

    /**
     * @Route("/new/person")
     * @param Request $request
     * @return Response
     */
    public function newPersonAction(Request $request): Response
    {
        $person = new Person();
        $personForm = $this->formFactory->create(PersonType::class, $person);
        $personForm->handleRequest($request);

        if ($personForm->isSubmitted() && $personForm->isValid()) {
            $this->entityManager->persist($person);
            $this->entityManager->flush();

            return RedirectResponse::create($this->router->generate(
                'app_contact_showcontact',
                ['uuid' => $person->getUuid()]
            ));
        }

        return Response::create(
            $this->twig->render(':contact:new_contact.html.twig', [
                'form' => $personForm->createView(),
            ])
        );
    }

    /**
     * @Route("/new/company")
     * @param Request $request
     * @return Response
     */
    public function newCompanyAction(Request $request): Response
    {
        $company = new Company();
        $companyForm = $this->formFactory->create(CompanyType::class, $company);
        $companyForm->handleRequest($request);

        if ($companyForm->isSubmitted() && $companyForm->isValid()) {
            $this->entityManager->persist($company);
            $this->entityManager->flush();

            return RedirectResponse::create($this->router->generate(
                'app_contact_showcontact',
                ['uuid' => $company->getUuid()]
            ));
        }

        return Response::create(
            $this->twig->render(':contact:new_contact.html.twig', [
                'form' => $companyForm->createView(),
            ])
        );
    }

    /**
     * @Route("/{uuid}")
     * @param Contact $contact
     * @return Response
     */
    public function showContactAction(Contact $contact): Response
    {
        return Response::create(
            $this->twig->render(':contact:show_contact.html.twig', [
                'contact' => $contact,
            ])
        );
    }

    /**
     * @Route("/")
     * @return Response
     */
    public function listContactsAction(): Response
    {
        $contactRepository = $this->entityManager->getRepository(Contact::class);

        return Response::create(
            $this->twig->render(':contact:list_contacts.html.twig', [
                'contacts' => $contactRepository->findAll(),
            ])
        );
    }
}
