<?php

namespace AppBundle\Entity;

interface NameableContactInterface
{
    /**
     * @return string
     */
    public function getName(): ?string;
}
