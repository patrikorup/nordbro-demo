<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{
    public function testListContactsPageIsOK()
    {
        $this->markTestSkipped('Skip test due to database testing is not set up.');

        $client = $this->createHttpsClient();

        $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testNewPersonPageIsOK()
    {
        $client = $this->createHttpsClient();

        $client->request('GET', '/new/person');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testNewCompanyPageIsOK()
    {
        $client = $this->createHttpsClient();

        $client->request('GET', '/new/company');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @return Client
     */
    protected function createHttpsClient(): Client
    {
        return static::createClient([], ['HTTPS' => true]);
    }
}
