<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Person;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{

    public function testThatNameIsFirstAndLastNameCombined()
    {
        $person = new Person();
        $person->setFirstName('john')
            ->setLastName('doe');

        $actual = $person->getName();

        $this->assertEquals('john doe', $actual);
    }
}
