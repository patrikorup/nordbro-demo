<?php

use Symfony\Component\HttpFoundation\Request;

require __DIR__.'/../vendor/autoload.php';

$env = getenv('SYMFONY_ENV');
if ($env === false) {
    throw new \Exception('The environment variable SYMFONY_ENV has not been set.');
}

$kernel = new AppKernel($env, $env === 'dev');

$request = Request::createFromGlobals();
Request::setTrustedProxies(
    [
        '127.0.0.1',
        $request->server->get('REMOTE_ADDR'),
    ]
);
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
